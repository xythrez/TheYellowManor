﻿using UnityEngine;
using System.Collections;
using YMLevel;
using YMUtils;
using YMPlayer;

public class ScriptPlayer : MonoBehaviour {

	private bool shouldMove = false;
	private ScriptInteractable interactableToExamine = null;
	private Vector2 targetLoc = new Vector2(0,0);
	private Vector2 movementDirection = new Vector2(0,0);
	private Animator playerAnimation;
	private static GameObject PLAYER_OBJECT = null;
	public static bool RESIZE = true;
	private float oldScale = 0f;

	// Use this for initialization
	void Start () {
	}
	
	//Update is called once per frame
	void Update () {
		float scale = GetScaleByHeight();
		transform.localScale = new Vector3((movementDirection.x < 0) ? -scale : scale, scale, scale);
		playerAnimation = gameObject.GetComponentInChildren<Animator>();

		if (shouldMove && !playerAnimation.GetCurrentAnimatorStateInfo(0).IsName("Pickup"))
		{
			playerAnimation.SetBool("Walking", true);

			if (Vector2.Distance(transform.position, targetLoc) <= Time.deltaTime * 12f) //If close enough, ignore boundaries
			{
				shouldMove = false;
				transform.position = new Vector3(targetLoc.x, targetLoc.y, GetLayerByHeight());

				if (interactableToExamine != null)
				{
					if (ScriptUIController.selectedItem != -1)
					{
						ScriptUIController.UpdateDisplayText(interactableToExamine.OnInteractableUse(ScriptUIController.selectedItem));
						ScriptUIController.selectedItem = -1;
						interactableToExamine = null;
					}
					else
					{
						ScriptUIController.UpdateDisplayText(interactableToExamine.OnInteractableExamine());
						interactableToExamine = null;
					}
				}
			}
			else if (IsTargetBlocked((Vector2)transform.position + movementDirection))
			{
				bool moved = false;
				if(!IsTargetBlocked((Vector2)transform.position + new Vector2(0f, ((movementDirection.y > 0f) ? 12f : -12f) * Time.deltaTime))) //If sideways blocked, up/down is blocked, move left/right. Re-calculate direction
				{
					//Debug.Log(IsTargetBlocked((Vector2)transform.position + movementDirection));
					transform.position += new Vector3(0f, ((movementDirection.y > 0f) ? 12f : -12f) * Time.deltaTime, 0f);
					MoveToSoftly(targetLoc);
					moved = true;
				}
				if(!IsTargetBlocked((Vector2)transform.position + new Vector2(((movementDirection.x > 0f) ? 12f : -12f) * Time.deltaTime, 0f))) //If sideways blocked, left/right is blocked, move up/down. Re-calculate direction
				{
					//Debug.Log(IsTargetBlocked((Vector2)transform.position + movementDirection));
					transform.position += new Vector3(((movementDirection.x > 0f) ? 12f : -12f) * Time.deltaTime, 0f, 0f);
					MoveToSoftly(targetLoc);
					moved = true;
				}
				if(moved == false)
				{
					transform.position += (Vector3)(movementDirection * Time.deltaTime * 12f);
				}

			} 
			else //If no other solution, ignore boundaries
			{
				transform.position += (Vector3)(movementDirection * Time.deltaTime * 12f);
			}

		}

		if (!shouldMove)
		{
			playerAnimation.SetBool("Walking", false);
		}
	}

	private bool IsTargetBlocked(Vector2 where)
	{
		int layerMask = 1 << 8;

		RaycastHit2D hit = Physics2D.Raycast(where, Vector2.zero, 0f, layerMask);
		if (hit)
		{
			if ((hit.collider && hit.collider.gameObject.tag == "Level") || (hit.collider && hit.collider.gameObject.tag == "Interactable"))
				return false;
		}
		return true;
	}

	private float GetLayerByHeight()
	{
		return transform.position.z;
	}

	private float GetScaleByHeight () {
		float deltaY = transform.position.y + 13f;
		float scale = RESIZE ? (1 - (deltaY * 0.03f)) : oldScale;
		oldScale = scale;
		return  scale;
	}

	public void MoveTo(Vector2 loc, GameObject interactable)
	{
		RESIZE = true;
		Vector2 direction = loc -  (Vector2) transform.position;
		direction.Normalize();
		movementDirection = direction;
		targetLoc = loc;
		shouldMove = true;
		if (interactable != null)
		{
			interactableToExamine = interactable.GetComponent<ScriptInteractable>();
			if (ScriptUIController.selectedItem != -1)
				ScriptUIController.UpdateDisplayText("format.use.txt", GameRegistry.INSTANCE.GetItem(ScriptUIController.selectedItem).GetItemName(), interactableToExamine.theInteractable.GetInteractableName());
			else
				ScriptUIController.UpdateDisplayText("format.moveto.txt", interactableToExamine.theInteractable.GetInteractableName());
		}
		else
		{
			interactableToExamine = null;
			if (ScriptUIController.selectedItem == -1)
				ScriptUIController.UpdateDisplayText("");
		}
			
	}

	public void MoveToSoftly (Vector2 loc)
	{
		Vector2 direction = loc - (Vector2)transform.position;
		direction.Normalize();
		movementDirection = direction;
	}

	public static void OnPickUp()
	{
		if (PLAYER_OBJECT == null)
			PLAYER_OBJECT = GameObject.FindGameObjectWithTag("Player");

		PLAYER_OBJECT.GetComponentInChildren<Animator>().SetTrigger("Pickup");
	}

}
