﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using YMLevel;

namespace YMPlayer {
	public class Player {

		public static Player THE_PLAYER = new Player();
		private List<int> inventory;

		public Player () {

			inventory = new List<int>();
		}

		public static void InitNewPlayer () {
			THE_PLAYER = new Player();
		}

		public void AddInventoryItem(int itemID, bool animate)
		{
			inventory.Add(itemID);
			ScriptUIController.ShowInventoryPickupFeedback(itemID);
			if (animate)
				ScriptPlayer.OnPickUp();
		}

		public void AddInventoryItem(int itemID)
		{
			this.AddInventoryItem(itemID, false);
		}

		public bool RemoveInventoryItem(int itemID)
		{
			return inventory.Remove(itemID);
		}

		public int[] GetInventory()
		{
			int[] formatted= new int[4];
			for (int i = 0; i < 4; i++)
				formatted[i] = -1;
			
			int[] inv = inventory.ToArray();
			for (int i = 0; i < ((inv.Length < 4) ? inv.Length : 4); i++)
				formatted[i] = inv[i];

			return formatted;
		}


	}
}
