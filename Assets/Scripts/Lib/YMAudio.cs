﻿using System;
using System.Collections;
using UnityEngine;

namespace YMAudio
{
	/**
	 * The AudioManager is a utility class that can be used to directly handle audio
	 * */
	public class AudioManager
	{
		public static readonly AudioManager INSTANCE = new AudioManager ();
		private readonly GameObject audioPref;

		public AudioManager () {
			this.audioPref = Resources.Load ("Prefab/Functional/AudioSource") as GameObject;
		}

		public AudioSource PlayAudio(AudioClip clip, GameObject location, bool loop, float volume) {
			GameObject newAudioPref = UnityEngine.Object.Instantiate (audioPref) as GameObject;
			AudioSource audioSrc = newAudioPref.GetComponent<AudioSource> ();


			audioSrc.loop = loop; //If it should loop
			audioSrc.clip = clip;//Assigns the clip
			audioSrc.volume = volume;

			newAudioPref.name = "AudioSource";
			newAudioPref.transform.position = location.transform.position;//Changes the position to the location
			newAudioPref.transform.SetParent(location.transform);//Sets it as a parent

			audioSrc.Play();

			return audioSrc;
		}

		public AudioSource PlayAudio (AudioClip clip, GameObject location, bool loop){
			return this.PlayAudio (clip, location, loop, 1.0F);
		}

		public AudioSource PlayAudio (AudioClip clip, GameObject location) {
			return this.PlayAudio (clip, location, false);
		}

		public AudioSource PlayAudio (AudioClip clip) {
			return this.PlayAudio (clip, Camera.main.gameObject);
		}

		public AudioSource PlayAudio (AudioClip clip, bool loop) {
			return this.PlayAudio (clip, Camera.main.gameObject, loop);
		}

		public AudioSource PlayAudio (AudioClip clip, bool loop, float volume) {
			return this.PlayAudio (clip, Camera.main.gameObject, loop, volume);
		}

		public AudioSource PlayAudio (AudioClip clip, float volume) {
			return this.PlayAudio (clip, Camera.main.gameObject, false, volume);
		}
	}
}

