﻿using System;
using UnityEngine;
using YMAudio;
using YMLevel;
using YMLib;
using YMPlayer;

namespace YMItem
{
	public class YMItems
	{
		public static bool initialized = false;

		public static void Init()
		{
			GameRegistry.INSTANCE.RegisterItem(new ItemTorch());
			GameRegistry.INSTANCE.RegisterItem(new ItemRope());
			GameRegistry.INSTANCE.RegisterItem(new ItemRopeTrap());
			GameRegistry.INSTANCE.RegisterItem(new ItemBearTrap());
			GameRegistry.INSTANCE.RegisterItem(new ItemKeyBookOne());
			GameRegistry.INSTANCE.RegisterItem(new ItemKeyBookThree());
			GameRegistry.INSTANCE.RegisterItem(new ItemKeyBookFour());
			GameRegistry.INSTANCE.RegisterItem(new ItemPliers());
			GameRegistry.INSTANCE.RegisterItem(new ItemCourtyardKey());
			GameRegistry.INSTANCE.RegisterItem(new ItemCheeseWedge());
			GameRegistry.INSTANCE.RegisterItem(new ItemMouseTrap());
			GameRegistry.INSTANCE.RegisterItem(new ItemRiggedMouseTrap());
			GameRegistry.INSTANCE.RegisterItem(new ItemDeadMouse());
			GameRegistry.INSTANCE.RegisterItem(new ItemLibKey());
			GameRegistry.INSTANCE.RegisterItem(new ItemBucket());
			initialized = true;
		}
	}

	public class ItemGeneric : IItem
	{
		protected Sprite itemSprite = null;

		public virtual int GetItemID()
		{
			throw new NotImplementedException();
		}

		public virtual string GetItemName()
		{
			throw new NotImplementedException();
		}

		public virtual string GetSpriteLocalName()
		{
			return "null";
		}

		public virtual Sprite GetItemSprite()
		{
			if (itemSprite == null)
				itemSprite = Resources.Load<Sprite>(string.Format("Texture/Item/{0}", GetSpriteLocalName()));
			return itemSprite;
		}

		public virtual string OnCombine(int itemID)
		{
			ScriptUIController.SetInventoryTextColor(Color.red);
			return string.Format("item.combine.fail.{0}.txt", UnityEngine.Random.Range(0, 5));
		}

		public virtual string OnExamine()
		{
			return "";
		}
	}

	public class ItemTorch : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.TORCH;
		}

		public override string GetItemName()
		{
			return "item.torch.name";
		}

		public override string GetSpriteLocalName()
		{
			return "Torch";
		}

		public override string OnExamine()
		{
			return "item.torch.examine.txt";
		}
	}

	public class ItemRope : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.ROPE;
		}

		public override string GetItemName()
		{
			return "item.rope.name";
		}

		public override string OnExamine()
		{
			return "item.rope.examine.txt";
		}

		public override string GetSpriteLocalName()
		{
			return "Rope";
		}

		public override string OnCombine(int itemID)
		{
			if (itemID == Items.BEAR_TRAP)
			{
				Player.THE_PLAYER.RemoveInventoryItem(GetItemID());
				Player.THE_PLAYER.RemoveInventoryItem(Items.BEAR_TRAP);
				Player.THE_PLAYER.AddInventoryItem(Items.ROPE_TRAP);
				ScriptUIController.SetInventoryTextColor(Color.green);
				return "item.combine.ropeandtrap.txt";
			}
			else
				return base.OnCombine(itemID);
		}
	}

	public class ItemBearTrap : ItemGeneric
	{

		public override int GetItemID()
		{
			return Items.BEAR_TRAP;
		}

		public override string GetItemName()
		{
			return "item.torture.name";
		}

		public override string OnExamine()
		{
			return "item.torture.examine.txt";
		}

		public override string GetSpriteLocalName()
		{
			return "BearTrap";
		}

		public override string OnCombine(int itemID)
		{
			if (itemID == Items.ROPE)
			{
				Player.THE_PLAYER.RemoveInventoryItem(GetItemID());
				Player.THE_PLAYER.RemoveInventoryItem(Items.ROPE);
				Player.THE_PLAYER.AddInventoryItem(3);
				ScriptUIController.SetInventoryTextColor(Color.green);
				return "item.combine.ropeandtrap.txt";
			}
			else
				return base.OnCombine(itemID);
		}
	}

	public class ItemRopeTrap : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.ROPE_TRAP;
		}

		public override string GetSpriteLocalName()
		{
			return "RopeTrap";
		}

		public override string GetItemName()
		{
			return "item.ropetrap.name";
		}

		public override string OnExamine()
		{
			return "item.ropetrap.examine.txt";
		}
	}

	public class ItemPliers : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.RUSTY_PLIERS;
		}

		public override string GetSpriteLocalName()
		{
			return "Pliers";
		}

		public override string GetItemName()
		{
			return "item.pliers.name";
		}

		public override string OnExamine()
		{
			return "item.pliers.examine.txt";
		}
	}

	public class ItemCourtyardKey : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.COURTYARD_KEY;
		}

		public override string GetSpriteLocalName()
		{
			return "CourtyardKey";
		}

		public override string GetItemName()
		{
			return "item.courtyardkey.name";
		}

		public override string OnExamine()
		{
			return "item.pliers.examine.txt";
		}
	}

	public class ItemKeyBookOne : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.KEY_BOOK_1;
		}

		public override string GetSpriteLocalName()
		{
			return "KeyBook_1";
		}

		public override string GetItemName()
		{
			return "item.booki.name";
		}

		public override string OnExamine()
		{
			return "item.booki.examine.txt";
		}
	}

	public class ItemKeyBookThree : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.KEY_BOOK_3;
		}

		public override string GetSpriteLocalName()
		{
			return "KeyBook_3";
		}

		public override string GetItemName()
		{
			return "item.bookiii.name";
		}

		public override string OnExamine()
		{
			return "item.bookiii.examine.txt";
		}
	}

	public class ItemKeyBookFour : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.KEY_BOOK_4;
		}

		public override string GetSpriteLocalName()
		{
			return "KeyBook_4";
		}

		public override string GetItemName()
		{
			return "item.bookiv.name";
		}

		public override string OnExamine()
		{
			return "item.bookiv.examine.txt";
		}
	}

	public class ItemCheeseWedge : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.CHEESE;
		}

		public override string GetSpriteLocalName()
		{
			return "cheese";
		}

		public override string GetItemName()
		{
			return "item.cheese.name";
		}

		public override string OnExamine()
		{
			return "item.cheese.examine.txt";
		}

		public override string OnCombine(int itemID)
		{
			if(itemID == Items.MOUSE_TRAP)
			{
				Player.THE_PLAYER.RemoveInventoryItem(Items.MOUSE_TRAP);
				Player.THE_PLAYER.RemoveInventoryItem(Items.CHEESE);
				Player.THE_PLAYER.AddInventoryItem(Items.RIGGED_MOUSE_TRAP);
				ScriptUIController.SetInventoryTextColor(Color.green);
				return "item.combine.cheeseandtrap.txt";
			}

			return base.OnCombine(itemID);
		}
	}

	public class ItemMouseTrap : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.MOUSE_TRAP;
		}

		public override string GetSpriteLocalName()
		{
			return "mousetrap";
		}

		public override string GetItemName()
		{
			return "item.mousetrap.name";
		}

		public override string OnExamine()
		{
			return "item.mousetrap.examine.txt";
		}

		public override string OnCombine(int itemID)
		{
			if (itemID == Items.CHEESE)
			{
				Player.THE_PLAYER.RemoveInventoryItem(Items.MOUSE_TRAP);
				Player.THE_PLAYER.RemoveInventoryItem(Items.CHEESE);
				Player.THE_PLAYER.AddInventoryItem(Items.RIGGED_MOUSE_TRAP);
				ScriptUIController.SetInventoryTextColor(Color.green);
				return "item.combine.cheeseandtrap.txt";
			}

			return base.OnCombine(itemID);
		}
	}

	public class ItemRiggedMouseTrap : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.RIGGED_MOUSE_TRAP;
		}

		public override string GetSpriteLocalName()
		{
			return "cheesetrap";
		}

		public override string GetItemName()
		{
			return "item.cheesetrap.name";
		}

		public override string OnExamine()
		{
			return "item.cheesetrap.examine.txt";
		}
	}

	public class ItemDeadMouse : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.DEAD_MOUSE;
		}

		public override string GetSpriteLocalName()
		{
			return "mouse";
		}

		public override string GetItemName()
		{
			return "item.mouse.name";
		}

		public override string OnExamine()
		{
			return "item.mouse.examine.txt";
		}

	}

	public class ItemLibKey : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.LIBRARY_KEY;
		}

		public override string GetSpriteLocalName()
		{
			return "librarykey";
		}

		public override string GetItemName()
		{
			return "item.librarykey.name";
		}

		public override string OnExamine()
		{
			return "item.library.examine.txt";
		}
	}

	public class ItemBucket : ItemGeneric
	{
		public override int GetItemID()
		{
			return Items.WATER_BUCKET;
		}

		public override string GetSpriteLocalName()
		{
			return "bucket";
		}

		public override string GetItemName()
		{
			return "item.bucket.name";
		}

		public override string OnExamine()
		{
			return "item.bucket.examine.txt";
		}
	}


}

