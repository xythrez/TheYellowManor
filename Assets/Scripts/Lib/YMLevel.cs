﻿using UnityEngine;
using YMPlayer;
using System.Collections.Generic;
using YMUtils;
using YMLevel;

namespace YMLevel
{

	public class GameRegistry {

		private Dictionary<int, IItem> itemRegistry = new Dictionary<int, IItem>();
		private Dictionary<int, IInteractable> interactableRegistry = new Dictionary<int, IInteractable>();
		private Dictionary<int, GameObject> levels = new Dictionary<int, GameObject>();
		private Dictionary<string, bool> gameCondition = new Dictionary<string, bool>();

		/**
		 * An instance of the GameRegistry class, used for registering item/interactable/game logic functions, also keeps track of all unloaded rooms
		 * */
		public static GameRegistry INSTANCE = new GameRegistry();

		/**
		 * Gets a boolean value of a game state, game states are saved across scenes and contain conditions for certain gameplay elements to be triggered
		 * */
		public bool GetGameState(string stateName) {
			bool result = false;
			gameCondition.TryGetValue(stateName, out result);
			return result;
		}

		public void ClearGameState()
		{
			gameCondition = new Dictionary<string, bool>();
		}

		/**
		 * Creates/changes a game state with the name provided, this game state can be called with GetGameState(string stateaName)
		 * */
		public void SetGameState(string stateName, bool state) {
			gameCondition.Add(stateName, state);
		}

		/**
		 * Registers an item for the game inventory, IItem is called when the player tries to combine/use/drop something
		 * */
		public void RegisterItem(IItem item) {
			itemRegistry.Add(item.GetItemID(), item);
		}
		
		/**
		 * Gets an item from the game's item registry, used to call an items OnDrop, OnExamine, and other methods
		 * */
		public IItem GetItem(int itemID) {
			IItem item = null;
			itemRegistry.TryGetValue(itemID, out item);
			return item;
		}

		/**
		 * Registers an item for the game scene, IInteractable is called when the player tries to use something in the world
		 * */
		public void RegisterInteractable(IInteractable interactable) {
			interactableRegistry.Add(interactable.GetInteractableID(), interactable);
		}

		/**
		 * Similar to GetItem
		 * */
		public IInteractable GetInteractable(int interactableID) {
			IInteractable interactable = null;
			interactableRegistry.TryGetValue(interactableID, out interactable);
			return interactable;
		}


		/**
		 * Registers a level of the game as a prefab, this is usually used in the init phrase of the game
		 * */
		public void RegisterLevel(GameObject level) {
			levels.Add(level.GetComponent<ScriptLevelInfo>().levelID, level);
		}

		/**
		 * Loads a level in the game
		 * */
		public void LoadLevel(int levelID, Vector2 moveTo) {
			ScriptUIController.AddDarkoutAnimation();
			GameObject old = GameObject.FindGameObjectWithTag("Level");

			if (old != null)
				GameObject.Destroy(old);

			//Cleans up the use function on level change
			ScriptUIController.selectedItem = -1;

			GameObject level = null;
			levels.TryGetValue(levelID, out level);
			GameLogicUtils.LoadLevel(level);

			GameObject player = GameObject.FindGameObjectWithTag("Player");
			player.transform.position = moveTo;
			player.GetComponent<ScriptPlayer>().MoveTo(moveTo, null);



		}

	}



	public interface IItem {

		string GetItemName();

		int GetItemID();

		Sprite GetItemSprite();

		string OnExamine();

		string OnCombine(int itemID);
	}

	public interface IInteractable {

		void OnLevelInit(GameObject interactable);

		void OnTick(GameObject interactable);

		string GetInteractableName();

		int GetInteractableID();

		string OnExamine(GameObject interactable);

		string OnUse(GameObject interactable, int itemID);
	}

	public static class YMLevels
	{
		public static bool initialized = false;

		public static void Init()
		{
			for (int i = 0; i < 8; i++)
			{
				GameObject level = Resources.Load<GameObject>(string.Format("Level/Level_{0}", i));
				if(level != null)
					GameRegistry.INSTANCE.RegisterLevel(level);
			}
			initialized = true;
		}
	}    

}
