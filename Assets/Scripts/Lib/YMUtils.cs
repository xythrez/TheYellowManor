﻿using System;
using System.Collections.Generic;
using UnityEngine;
using YMLevel;

namespace YMUtils
{
	public static class SystemUtils
	{
	}

	public static class IOUtils
	{
		public static string[] ReadTextAsLines(string path)
		{
			return Resources.Load<TextAsset>(path).text.Split('\n');
		}
	}

	public class LanguageMapping
	{

		public static LanguageMapping INSTANCE = new LanguageMapping("Lang/en_US");

		private Dictionary<string, string> map = new Dictionary<string, string>();

		public LanguageMapping(string path)
		{
			string[] lines = IOUtils.ReadTextAsLines(path);
			List<string> translatable = new List<string>();

			foreach (string line in lines)
			{
				if (line != null && line != "" && !line.Contains("##") && line.Contains("=") && line.IndexOf('=') != 0 && line.IndexOf('=') + 1 != line.Length)
					translatable.Add(line);
			}

			ParseLangFile(translatable.ToArray());
		}

		private void ParseLangFile(string[] lang)
		{
			foreach (string line in lang)
			{
				string[] processed = line.Split('=');

				if (map.ContainsKey(processed[0].ToLower())) { 
					map.Remove(processed[0].ToLower());
					Debug.Log(String.Format("[Error] Found localization messages for the same key: '{0}'", processed[0]));
				}

				map.Add(processed[0].ToLower(), processed[1]);
					
			}

			map.Add("", "");//Just to make our lives easier, don't even TRY to override this
		}

		/**
		 * The translate function is used to query localized text with unlocalized text,
		 * It would be best to format the unlocalized text as "item.something.name" or "button.somethingElse.desciption"
		 * */
		public string Translate(string unlocalized)
		{
			unlocalized = unlocalized.ToLower();
			string localized = unlocalized;
			map.TryGetValue(unlocalized, out localized);
			if (localized == null)
			{
				Debug.Log(string.Format("[Error] Cannot find localized text for localization key: '{0}'", unlocalized));
				return unlocalized;
			}
			return localized;
		}

		public string TranslateFormatted(string format, params string[] unlocalized)
		{
			string[] localized = new string[unlocalized.Length];
			for (int i = 0; i < unlocalized.Length; i++)
				localized[i] = Translate(unlocalized[i]);
			return string.Format(Translate(format), localized);
		}
	}

	public class AudioUtils
	{
		public static AudioClip PlayRandom(AudioClip[] clips)
		{
			return clips[UnityEngine.Random.Range(0, clips.Length)];
		}
	}

	public class GameLogicUtils {
		public static void LoadLevel(GameObject levelPref)
		{
			
			GameObject level = GameObject.Instantiate<GameObject>(levelPref);
			level.name = "Level";
			level.transform.position = new Vector3(0, 0, 1);
		}
	}

}

