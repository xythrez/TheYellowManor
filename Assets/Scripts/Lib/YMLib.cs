﻿using System;
using UnityEngine;

namespace YMLib {
	public static class Reference {
		
	}

	public enum EnumInventoryFunction
	{
		EXAMINE,
		USE,
		COMBINE,
		NONE
	}

	public static class AudioClips
	{
		public static readonly AudioClip UI_BUTTON = Resources.Load<AudioClip>("Audio/Effects/uisounds/button");
		public static readonly AudioClip UI_FAIL = Resources.Load<AudioClip>("Audio/Effects/uisounds/failure");
		public static readonly AudioClip UI_SUCCEED = Resources.Load<AudioClip>("Audio/Effects/uisounds/success");
		public static readonly AudioClip UI_PICKUP = Resources.Load<AudioClip>("Audio/Effects/uisounds/pickup");

		public static readonly AudioClip[] BG = new AudioClip[] { Resources.Load<AudioClip>("Audio/Backgrounds/bg_1"), Resources.Load<AudioClip>("Audio/Backgrounds/bg_2")};
		public static readonly AudioClip TRAP_HIT = Resources.Load<AudioClip>("Audio/Effects/trap_hit");
		public static readonly AudioClip WOODEN_DOOR_OPEN = Resources.Load<AudioClip>("Audio/Effects/wooden_door");
		public static readonly AudioClip WOODEN_DOOR_CLOSE = Resources.Load<AudioClip>("Audio/Effects/doorclose");
		public static readonly AudioClip CHAIN_BREAK = Resources.Load<AudioClip>("Audio/Effects/chain");
		public static readonly AudioClip CLOCK_TICK = Resources.Load<AudioClip>("Audio/Effects/clocktick");
		public static readonly AudioClip STATUE_CRUMBLE = Resources.Load<AudioClip>("Audio/Effects/crumble");
		public static readonly AudioClip DOOR_UNLOCK = Resources.Load<AudioClip>("Audio/Effects/locksound");
		public static readonly AudioClip BOX_PUSH = Resources.Load<AudioClip>("Audio/Effects/push");
		public static readonly AudioClip MOUSETRAP_SNAP = Resources.Load<AudioClip>("Audio/Effects/mousetrap");
		public static readonly AudioClip LOCKED = Resources.Load<AudioClip>("Audio/Effects/doorjam");
		public static readonly AudioClip LOCKTICK = Resources.Load<AudioClip>("Audio/Effects/lockticking");


		public static readonly AudioClip[] YELL = new AudioClip[] {
			Resources.Load<AudioClip>("Audio/Effects/yell/yell1"),
			Resources.Load<AudioClip>("Audio/Effects/yell/yell2"),
			Resources.Load<AudioClip>("Audio/Effects/yell/yell3")
		};

		public static readonly AudioClip[] MUMBLE = new AudioClip[]
		{
			Resources.Load<AudioClip>("Audio/Effects/mumbling/mumble1"),
			Resources.Load<AudioClip>("Audio/Effects/mumbling/mumble2"),
			Resources.Load<AudioClip>("Audio/Effects/mumbling/mumble3")
		};

		public static readonly AudioClip[] GUARD = new AudioClip[]
		{
			Resources.Load<AudioClip>("Audio/Effects/guard/guard1"),
			Resources.Load<AudioClip>("Audio/Effects/guard/guard2"),
			Resources.Load<AudioClip>("Audio/Effects/guard/guard3")
		};

	}

	public static class Items
	{
		public static readonly int TORCH = 0;
		public static readonly int ROPE = 1;
		public static readonly int BEAR_TRAP = 2;
		public static readonly int ROPE_TRAP = 3;
		public static readonly int CHEESE = 4;
		public static readonly int MOUSE_TRAP = 5;
		public static readonly int RIGGED_MOUSE_TRAP = 6;
		public static readonly int DEAD_MOUSE = 7;
		public static readonly int LIBRARY_KEY = 8;
		public static readonly int RUSTY_PLIERS = 9;
		public static readonly int KEY_BOOK_1 = 10;
		public static readonly int KEY_BOOK_3 = 11;
		public static readonly int KEY_BOOK_4 = 12;
		public static readonly int COURTYARD_KEY = 13;
		public static readonly int WATER_BUCKET = 14;
	}

	public static class Interactables
	{
		public static readonly int CELL_WINDOW = 0;
		public static readonly int TORCH = 1;
		public static readonly int HOOK_DECORATIONAL = 2;
		public static readonly int BEAR_TRAP = 3;
		public static readonly int STRAW_PILE = 4;
		public static readonly int CELL_DOOR = 5;
		public static readonly int OLD_MAN = 6;
		public static readonly int HOOK_INTERACTABLE = 7;
		public static readonly int ROPE_TRAP = 8;
		public static readonly int KNOCKED_OUT_GUARD = 9;
		public static readonly int BLOOD_STAIN = 15;
		public static readonly int COURTYARD_KEY = 16;
		public static readonly int STORYBOOK = 17;
		public static readonly int MINICLOCK = 18;
		public static readonly int CANDLE_STICK = 19;
		public static readonly int PAINTING_1 = 20;
		public static readonly int PAINTING_2 = 21;
		public static readonly int PAINTING_3 = 22;
		public static readonly int PAINTING_4 = 23;
		public static readonly int PAINTING_5 = 24;
		public static readonly int MECH_BOOKSHELF = 25;
		public static readonly int PODIUM = 26;
		public static readonly int LIB_KIT_DOOR = 27;
		public static readonly int LIB_COURT_DOOR = 28;
		public static readonly int CARDBOARD_BOX = 29;
		public static readonly int STATUE = 30;
		public static readonly int FIREPLACE = 31;
		public static readonly int FIREPLACE_TOOLS = 32;
		public static readonly int GRANDFATHER_CLOCK = 33;
		public static readonly int OFFICE_DRAWERS = 34;
		public static readonly int HIDDEN_ROOM = 35;
		public static readonly int KIT_CELL_DOOR = 36;
		public static readonly int KIT_LIB_DOOR = 37;
		public static readonly int CRATE = 38;
		public static readonly int CHEESE_WHEEL = 39;
		public static readonly int CAT = 40;
		public static readonly int MOUSETRAP = 41;
		public static readonly int MOUSETRAP_RIGGED = 42;
		public static readonly int MOUSE_HOLE = 43;
		public static readonly int SHELF_CHEESE = 44;
		public static readonly int SHELF = 45;
		public static readonly int BARRED_CELL = 46;
		public static readonly int COAT_RACK = 47;
		public static readonly int CUPBOARD = 48;
		public static readonly int WATER_BUCKET = 49;
		public static readonly int PENTAGRAM = 50;
		public static readonly int PUDDLE = 51;
		public static readonly int GATE = 52;
		public static readonly int DIAL_1 = 53;
		public static readonly int DIAL_2 = 54;
		public static readonly int DIAL_3 = 55;
		public static readonly int UNLOCK_BUTTON = 56;
		public static readonly int CHAIR = 57;
		public static readonly int TABLE = 58;
		public static readonly int RETURN_TXT = 59;
	}

	public static class Levels
	{
		public static readonly int CELLAR = 0;
		public static readonly int KITCHEN_RIGHT = 1;
		public static readonly int KITCHEN_LEFT = 2;
		public static readonly int LIBRARY_RIGHT = 3;
		public static readonly int LIBRARY_LEFT = 4;
		public static readonly int LIBRARY_OFFICE = 5;
		public static readonly int COURTYARD = 6;
		public static readonly int LOCK = 7;
	}
}

