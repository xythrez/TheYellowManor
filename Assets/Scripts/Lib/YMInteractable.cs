﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using YMAudio;
using YMLevel;
using YMLib;
using YMPlayer;
using YMUtils;

namespace YMInteractable
{
	public class YMInteractables
	{
		public static bool initialized = false;

		public static void Init()
		{
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCellWindow());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableHook());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableStraw());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCatatonic());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCellarDoor());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableTorch());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableHookDeco());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableBearTrap());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableRopeTrap());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableGuard());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableBloodStain());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCourtyardKey());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableStorybook());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableMiniClock());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCandleStick());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractablePaintingOne());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractablePaintingTwo());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractablePaintingThree());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractablePaintingFour());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractablePaintingFive());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableMechBookshelf());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractablePodium());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableLibKitDoor());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableLibCourtDoor());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCardboardBox());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableStatue());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableFireplace());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableFireplaceTools());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableGrandfatherClock());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableOfficeDrawers());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableHiddenRoom());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableKitCellDoor());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableKitLibDoor());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCrate());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCheeseWheel());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCat());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableMouseTrap());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableRiggedMouseTrap());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableMouseHole());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableShelfCheese());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableShelf());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableBarredCell());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCoatRack());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableCupboard());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableBucket());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractablePentagram());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractablePuddle());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableGate());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableDialOne());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableDialTwo());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableDialThree());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableUnlockButton());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableTable());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableChair());
			GameRegistry.INSTANCE.RegisterInteractable(new InteractableReturn());

			initialized = true;

		}
	}

	public class InteractableGeneric : IInteractable
	{
		public virtual void OnLevelInit(GameObject interactable)
		{
		}

		public virtual void OnTick(GameObject interactable)
		{
		}

		public virtual int GetInteractableID()
		{
			throw new NotImplementedException();
		}

		public virtual string GetInteractableName()
		{
			return "";
		}

		public virtual string OnExamine(GameObject obj)
		{
			return "";
		}

		public virtual string OnUse(GameObject interactable, int itemID)
		{
			ScriptUIController.SetDisplayTextColor(Color.red);
			return string.Format("item.combine.fail.{0}.txt", UnityEngine.Random.Range(0, 5));
		}
	}

	public class InteractableCellWindow : InteractableGeneric
	{
		public override int GetInteractableID()
		{
			return Interactables.CELL_WINDOW;
		}

		public override string GetInteractableName()
		{
			return "interactable.cellwindow.name";
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.cellwindow.examine.txt";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if (itemID == Items.ROPE)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.cellwindow.userope.txt";
			}
			else
				return base.OnUse(interactable, itemID);

		}

	}

	public class InteractableHookDeco : InteractableGeneric
	{
		public override int GetInteractableID()
		{
			return Interactables.HOOK_DECORATIONAL;
		}

		public override string GetInteractableName()
		{
			return "interactable.hook.name";
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.hookdeco.examine.txt";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if (itemID == Items.ROPE_TRAP)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.hookdeco.useropetrap.txt";
			}
			else
				return base.OnUse(interactable, itemID);
		}
	}

	public class InteractableHook : InteractableGeneric
	{
		public override int GetInteractableID()
		{
			return Interactables.HOOK_INTERACTABLE;
		}

		public override string GetInteractableName()
		{
			return "interactable.hook.name";
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.hook.examine.txt";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if (itemID == Items.ROPE_TRAP)
			{
				Player.THE_PLAYER.RemoveInventoryItem(Items.ROPE_TRAP);
				GameRegistry.INSTANCE.SetGameState("trap_set", true);
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "combine.hookwithropetrap.txt";
			}
			else
				return base.OnUse(interactable, itemID);
		}
	}

	public class InteractableCatatonic : InteractableGeneric
	{
		private Sprite QE_1 = null;
		private Sprite QE_2 = null;
		private ScriptExtraAudio gc = null;

		public override void OnLevelInit(GameObject interactable)
		{
			gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<ScriptExtraAudio>();

			if (QE_1 == null)
				QE_1 = Resources.Load<Sprite>("Texture/QuickEvent/qe_01_0");
			if (QE_2 == null)
				QE_2 = Resources.Load<Sprite>("Texture/QuickEvent/qe_01_1");
		}

		public override int GetInteractableID()
		{
			return Interactables.OLD_MAN;
		}
		public override string GetInteractableName()
		{
			return "interactable.catatonic.name";
		}

		public override string OnExamine(GameObject interactable)
		{
			AudioManager.INSTANCE.PlayAudio(AudioUtils.PlayRandom(AudioClips.MUMBLE));
			return "interactable.catatonic.examine.txt";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if (itemID == Items.TORCH)
			{
				AudioManager.INSTANCE.PlayAudio(AudioUtils.PlayRandom(AudioClips.YELL));
				gc.Invoke("GuardCall", 1);
				ScriptUIController.SetDisplayTextColor(Color.green);
				interactable.GetComponentInChildren<Animator>().SetTrigger("Scream");

				if (GameRegistry.INSTANCE.GetGameState("trap_set"))
				{
					Player.THE_PLAYER.RemoveInventoryItem(Items.TORCH);
					GameRegistry.INSTANCE.SetGameState("cellar_complete", true);
					ScriptUIController.SetAudioOnEventChange(AudioClips.WOODEN_DOOR_OPEN, AudioClips.TRAP_HIT);
					ScriptUIController.DisplayQuickEvent(QE_1, QE_2);
					return "interactable.catatonic.dooropen.txt";
				}
				else
				{
					gc.Invoke("CloseDoor", 1.5f);
					((InteractableCellarDoor)GameRegistry.INSTANCE.GetInteractable(Interactables.CELL_DOOR)).animDoor.SetTrigger("Open");
				}
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.catatonic.usetorch.txt";
			}
			else if (itemID == Items.BEAR_TRAP)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.catatonic.usetorture.txt";
			}
			else if (itemID == Items.ROPE)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.catatonic.userope.txt";
			}
			else if (itemID == Items.ROPE_TRAP)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.catatonic.usetrap.txt";
			}
			else
			{
				return base.OnUse(interactable, itemID);
			}
		}
	}

	public class InteractableStraw : InteractableGeneric
	{
		public override int GetInteractableID()
		{
			return Interactables.STRAW_PILE;
		}

		public override string GetInteractableName()
		{
			return "interactable.straw.name";
		}

		public override string OnExamine(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("found_rope"))
				return "interactable.straw.examine.txt";
			else {
				Player.THE_PLAYER.AddInventoryItem(1, true);
				GameRegistry.INSTANCE.SetGameState("found_rope", true);
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.rope.examine.txt";
			}
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if (itemID == Items.TORCH)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.straw.usetorch.txt";
			}
			else
				return base.OnUse(interactable, itemID);
		}
	}

	public class InteractableCellarDoor : InteractableGeneric
	{
		//        bool stopDetect = false;

		public Animator animDoor = null;

		public override int GetInteractableID()
		{
			return Interactables.CELL_DOOR;
		}

		public override void OnTick(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("cellar_complete"))
			{
				GameObject.Destroy(animDoor);
				interactable.GetComponentInChildren<SpriteRenderer>().sprite = Resources.Load<Sprite>("Texture/Interactable/cell_door_01");
			}
		}

		public override void OnLevelInit(GameObject interactable)
		{
			animDoor = interactable.GetComponentInChildren<Animator>();
		}

		public override string GetInteractableName()
		{
			return "interactable.door.name";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if(itemID == Items.TORCH)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.door.usetorch.txt";
			}
			else
				return base.OnUse(interactable, itemID);
		}

		public override string OnExamine(GameObject interactable)
		{   if(GameRegistry.INSTANCE.GetGameState("cellar_complete"))
			{
				AudioManager.INSTANCE.PlayAudio(AudioClips.WOODEN_DOOR_CLOSE);
				GameRegistry.INSTANCE.LoadLevel(Levels.KITCHEN_RIGHT, new Vector3(17f, -10.45f, -2f));
				return "";
			}
			AudioManager.INSTANCE.PlayAudio(AudioClips.LOCKED);
			return "interactable.door.examine.txt";
		}
	}


	public class InteractableTorch : InteractableGeneric
	{
		public override void OnLevelInit(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("cellar_complete"))
				GameObject.Destroy(interactable);
		}

		public override string GetInteractableName()
		{
			return "interactable.torch.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.TORCH;
		}

		public override string OnExamine(GameObject interactable)
		{
			Player.THE_PLAYER.AddInventoryItem(0, true);
			UnityEngine.Object.Destroy(interactable);
			ScriptUIController.SetDisplayTextColor(Color.green);
			return "interactable.torch.examine.txt";
		}
	}

	public class InteractableBearTrap : InteractableGeneric
	{
		public override void OnLevelInit(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("cellar_complete"))
				GameObject.Destroy(interactable);
		}

		public override string GetInteractableName()
		{
			return "interactable.torture.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.BEAR_TRAP;
		}

		public override string OnExamine(GameObject interactable)
		{
			Player.THE_PLAYER.AddInventoryItem(2, true);
			UnityEngine.Object.Destroy(interactable);
			ScriptUIController.SetDisplayTextColor(Color.green);
			return "interactable.torture.examine.txt";
		}
	}

	public class InteractableRopeTrap : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.ropetrap.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.ROPE_TRAP;
		}

		public override void OnTick(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("cellar_complete"))
				GameObject.Destroy(interactable);
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.ropetrap.examine.txt";
		}
	}

	public class InteractableGuard : InteractableGeneric
	{
		private Sprite QE_1 = null;

		public override void OnLevelInit(GameObject interactable)
		{
			if (QE_1 == null)
				QE_1 = Resources.Load<Sprite>("Texture/QuickEvent/qe_01_1"); 
		}

		public override string GetInteractableName()
		{
			return "interactable.guard.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.KNOCKED_OUT_GUARD;
		}

		public override string OnExamine(GameObject interactable)
		{
			ScriptUIController.DisplayQuickEvent(QE_1);
			return "interactable.guard.examine.txt";
		}
	}

	public class InteractableBloodStain : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.bloodstain.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.BLOOD_STAIN;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.bloodstain.examine.txt";
		}
	}

	public class InteractableCourtyardKey : InteractableGeneric
	{
		public override void OnLevelInit(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("key_courtyard"))
				GameObject.Destroy(interactable);
		}

		public override string GetInteractableName()
		{
			return "interactable.courtyardkey.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.COURTYARD_KEY;
		}

		public override string OnExamine(GameObject interactable)
		{
			Player.THE_PLAYER.AddInventoryItem(Items.COURTYARD_KEY, true);
			GameRegistry.INSTANCE.SetGameState("key_courtyard", true);
			GameObject.Destroy(interactable);
			ScriptUIController.SetDisplayTextColor(Color.green);
			return "interactable.courtyardkey.examine.txt";
		}
	}

	public class InteractableStorybook : InteractableGeneric
	{
		private Sprite QE_1;
		private Sprite QE_2;
		private Sprite QE_3;
		private Sprite QE_4;

		public override void OnLevelInit(GameObject interactable)
		{
			if(QE_1 == null || QE_2 == null || QE_3 == null || QE_4 == null)
			{
				QE_1 = Resources.Load<Sprite>("Texture/QuickEvent/qe_05_0");
				QE_2 = Resources.Load<Sprite>("Texture/QuickEvent/qe_05_1");
				QE_3 = Resources.Load<Sprite>("Texture/QuickEvent/qe_05_2");
				QE_4 = Resources.Load<Sprite>("Texture/QuickEvent/qe_05_3");

			}
		}

		public override string GetInteractableName()
		{
			return "interactable.storybook.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.STORYBOOK;
		}

		public override string OnExamine(GameObject interactable)
		{
			ScriptUIController.DisplayQuickEvent(QE_1, QE_2, QE_3, QE_4);
			return "interactable.storybook.examine.txt";
		}
	}

	public class InteractableMiniClock : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.miniclock.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.MINICLOCK;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.miniclock.examine.txt";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if (itemID == Items.COURTYARD_KEY) 
			{
				AudioManager.INSTANCE.PlayAudio(AudioClips.DOOR_UNLOCK);
				Player.THE_PLAYER.RemoveInventoryItem(Items.COURTYARD_KEY);
				ScriptUIController.SetDisplayTextColor(Color.green);
				GameRegistry.INSTANCE.SetGameState("lib_complete", true);
				return "interactable.miniclock.usekey.txt";
			}
			return base.OnUse(interactable, itemID);
		}
	}

	public class InteractableCandleStick : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.candlestick.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.CANDLE_STICK;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.candlestick.examine.txt";
		}
	}

	public class InteractablePaintingOne : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.painting.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.PAINTING_1;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.painting.1.examine.txt";
		}
	}

	public class InteractablePaintingTwo : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.painting.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.PAINTING_2;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.painting.1.examine.txt";
		}
	}

	public class InteractablePaintingThree : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.painting.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.PAINTING_3;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.painting.2.examine.txt";
		}
	}

	public class InteractablePaintingFour : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.painting.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.PAINTING_4;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.painting.3.examine.txt";
		}
	}

	public class InteractablePaintingFive : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.painting.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.PAINTING_5;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.painting.4.examine.txt";
		}
	}

	public class InteractableMechBookshelf : InteractableGeneric
	{
		private Sprite shelf_stage_0 = null;
		private Sprite shelf_stage_1 = null;
		private Sprite shelf_stage_2 = null;
		private Sprite shelf_stage_3 = null;
		private Sprite shelf_stage_4 = null; 

		public override string GetInteractableName()
		{
			return "interactable.mechbookshelf.name";
		}

		public override void OnLevelInit(GameObject interactable)
		{
			if(shelf_stage_0 == null || shelf_stage_1 == null || shelf_stage_2 == null || shelf_stage_3 == null || shelf_stage_4 == null)
			{
				shelf_stage_0 = Resources.Load<Sprite>("Texture/Interactable/mech_shelf_0"); // Chained
				shelf_stage_1 = Resources.Load<Sprite>("Texture/Interactable/mech_shelf_1"); // Unchained
				shelf_stage_2 = Resources.Load<Sprite>("Texture/Interactable/mech_shelf_2"); // Book i placed
				shelf_stage_3 = Resources.Load<Sprite>("Texture/Interactable/mech_shelf_3"); // Book iii placed
				shelf_stage_4 = Resources.Load<Sprite>("Texture/Interactable/mech_shelf_4"); // All books placed
			}

			SpriteRenderer sr = interactable.GetComponentInChildren<SpriteRenderer>();
			if (GameRegistry.INSTANCE.GetGameState("placed_bookiv"))
			{
				GameObject.Destroy(interactable.GetComponent<Animator>());
				interactable.transform.position += new Vector3(5.0f, 0);
				sr.sprite = shelf_stage_4;
			}
			else if (GameRegistry.INSTANCE.GetGameState("placed_bookiii"))
				sr.sprite = shelf_stage_3;
			else if (GameRegistry.INSTANCE.GetGameState("placed_booki"))
				sr.sprite = shelf_stage_2;
			else if (GameRegistry.INSTANCE.GetGameState("shelf_unchained"))
				sr.sprite = shelf_stage_1;

		}

		public override int GetInteractableID()
		{
			return Interactables.MECH_BOOKSHELF;
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			SpriteRenderer sr = interactable.GetComponentInChildren<SpriteRenderer>();

			if (itemID == Items.RUSTY_PLIERS)
			{
				sr.sprite = shelf_stage_1;
				GameRegistry.INSTANCE.SetGameState("shelf_unchained", true);
				Player.THE_PLAYER.RemoveInventoryItem(itemID);
				AudioManager.INSTANCE.PlayAudio(AudioClips.CHAIN_BREAK);
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.mechbookshelf.use.pliers.txt";
			}
			else if (itemID == Items.KEY_BOOK_1 && GameRegistry.INSTANCE.GetGameState("shelf_unchained"))
			{
				sr.sprite = shelf_stage_2;
				AudioManager.INSTANCE.PlayAudio(AudioClips.DOOR_UNLOCK);
				GameRegistry.INSTANCE.SetGameState("placed_booki", true);
				Player.THE_PLAYER.RemoveInventoryItem(itemID);
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.mechbookshelf.use.booki.txt";
			}
			else if (itemID == Items.KEY_BOOK_1)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.mechbookshelf.use.locked.txt";
			}
			else if (itemID == Items.KEY_BOOK_3)
			{
				sr.sprite = shelf_stage_3;
				AudioManager.INSTANCE.PlayAudio(AudioClips.STATUE_CRUMBLE);
				GameRegistry.INSTANCE.SetGameState("placed_bookiii", true);
				Player.THE_PLAYER.RemoveInventoryItem(itemID);
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.mechbookshelf.use.bookiii.txt";
			}
			else if (itemID == Items.KEY_BOOK_4)
			{
				sr.sprite = shelf_stage_4;
				AudioManager.INSTANCE.PlayAudio(AudioClips.BOX_PUSH);
				GameRegistry.INSTANCE.SetGameState("placed_bookiv", true);
				Player.THE_PLAYER.RemoveInventoryItem(itemID);
				ScriptUIController.SetDisplayTextColor(Color.green);
				interactable.GetComponent<Animator>().SetTrigger("Push");
				//interactable.transform.position += new Vector3(5.0f, 0, 0);
				return "interactable.mechbookshelf.use.bookiv.txt";
			} else
				return base.OnUse(interactable, itemID);
		}

		public override string OnExamine(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("placed_bookiv"))
				return "interactable.mechbookshelf.examine.complete.txt";
			else if (GameRegistry.INSTANCE.GetGameState("placed_bookiii"))
				return "interactable.mechbookshelf.examine.unchained.1.txt";
			else if (GameRegistry.INSTANCE.GetGameState("placed_booki"))
				return "interactable.mechbookshelf.examine.unchained.2.txt";
			else if (GameRegistry.INSTANCE.GetGameState("shelf_unchained"))
				return "interactable.mechbookshelf.examine.unchained.3.txt";
			return "interactable.mechbookshelf.examine.chained.txt";
		}
	}

	public class InteractablePodium : InteractableGeneric
	{
		//GameObject podiumCase = GameObject.Find("podium_case");
		GameObject bookiii = null; 

		public override void OnLevelInit(GameObject interactable)
		{
			bookiii = GameObject.Find("podium_book");
			if (GameRegistry.INSTANCE.GetGameState("found_bookiii"))
				bookiii.SetActive(false);
		}

		public override string GetInteractableName()
		{
			return "interactable.podium.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.PODIUM;
		}

		public override string OnExamine(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("found_bookiii"))
				return "interactable.podium.examine.complete.txt";
			else if (GameRegistry.INSTANCE.GetGameState("placed_booki"))
			{
				Player.THE_PLAYER.AddInventoryItem(Items.KEY_BOOK_3, true);
				ScriptUIController.SetDisplayTextColor(Color.green);
				GameRegistry.INSTANCE.SetGameState("found_bookiii", true);
				bookiii.SetActive(false);
				return "interactable.podium.examine.unlocked.txt";
			}
			return "interactable.podium.examine.locked.txt";

		}
	}

	public class InteractableLibKitDoor : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.libkitdoor.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.LIB_KIT_DOOR;
		}

		public override string OnExamine(GameObject interactable)
		{
			GameRegistry.INSTANCE.LoadLevel(Levels.KITCHEN_RIGHT, new Vector3(3f, 0f, -2f));
			AudioManager.INSTANCE.PlayAudio(AudioClips.WOODEN_DOOR_OPEN);
			return "";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			return "";
		}
	}

	public class InteractableLibCourtDoor : InteractableGeneric
	{
		private GameObject spriteDoorOpen = null;

		public override void OnLevelInit(GameObject interactable)
		{
			spriteDoorOpen = GameObject.Find("sprite_lib_court_door");
			if (!GameRegistry.INSTANCE.GetGameState("lib_complete"))
				spriteDoorOpen.SetActive(false);

		}

		public override string GetInteractableName()
		{
			return "interactable.libcourtdoor.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.LIB_COURT_DOOR;
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if(itemID == Items.COURTYARD_KEY)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.libcourtdoor.usekey.txt";
			}

			return base.OnUse(interactable, itemID);
		}

		public override string OnExamine(GameObject interactable)
		{
			if (!GameRegistry.INSTANCE.GetGameState("lib_complete"))
			{
				AudioManager.INSTANCE.PlayAudio(AudioClips.LOCKED);
				return "interactable.libcourtdoor.examine.txt";
			}
			AudioManager.INSTANCE.PlayAudio(AudioClips.WOODEN_DOOR_CLOSE);
			GameRegistry.INSTANCE.LoadLevel(Levels.COURTYARD, new Vector3(0f, -10.45f, -2f));
			return ""; 

		}
	}

	public class InteractableCardboardBox : InteractableGeneric
	{
		Sprite open = Resources.Load<Sprite>("Texture/Interactable/BoxOpened");

		public override void OnLevelInit(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("found_booki"))
				interactable.GetComponentInChildren<SpriteRenderer>().sprite = open;
		}

		public override string GetInteractableName()
		{
			return "interactable.cardboardbox.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.CARDBOARD_BOX;
		}

		public override string OnExamine(GameObject interactable)
		{
			if(GameRegistry.INSTANCE.GetGameState("found_booki"))
				return "interactable.cardboardbox.examine.open.txt";

			Player.THE_PLAYER.AddInventoryItem(Items.KEY_BOOK_1, true);
			GameRegistry.INSTANCE.SetGameState("found_booki", true);
			interactable.GetComponentInChildren<SpriteRenderer>().sprite = open;
			ScriptUIController.SetDisplayTextColor(Color.green);
			return "interactable.cardboardbox.examine.closed.txt";
		}
	}

	public class InteractableStatue : InteractableGeneric
	{
		Sprite shattered = Resources.Load<Sprite>("Texture/Interactable/shattered");
		Sprite withBook = Resources.Load<Sprite>("Texture/Interactable/withBook");

		public override void OnLevelInit(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("found_bookiv"))
				interactable.GetComponentInChildren<SpriteRenderer>().sprite = shattered;
			else if (GameRegistry.INSTANCE.GetGameState("placed_bookiii"))
				interactable.GetComponentInChildren<SpriteRenderer>().sprite = withBook;

		}


		public override string GetInteractableName()
		{
			if (GameRegistry.INSTANCE.GetGameState("placed_bookiii"))
				return "interactable.statue.shattered.name";
			return "interactable.statue.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.STATUE;
		}

		public override string OnExamine(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("found_bookiv"))
				return "interactable.statue.examine.none.txt";
			else if (GameRegistry.INSTANCE.GetGameState("placed_bookiii")) {
				interactable.GetComponentInChildren<SpriteRenderer>().sprite = shattered;
				Player.THE_PLAYER.AddInventoryItem(Items.KEY_BOOK_4);
				ScriptUIController.SetDisplayTextColor(Color.green);
				GameRegistry.INSTANCE.SetGameState("found_bookiv", true);
				return "interactable.statue.examine.shattered.txt";
			}
			return "interactable.statue.examine.initial.txt";
		}
	}

	public class InteractableFireplace : InteractableGeneric
	{
		private GameObject spriteFire = null;
		private Sprite QE_1 = null;

		public override void OnLevelInit(GameObject interactable)
		{
			spriteFire = GameObject.Find("sprite_fire");

			if (QE_1 == null)
				QE_1 = Resources.Load<Sprite>("Texture/QuickEvent/qe_03_0");

			if (GameRegistry.INSTANCE.GetGameState("fire_out"))
				GameObject.Destroy(spriteFire);
		}

		public override string GetInteractableName()
		{
			return "interactable.fireplace.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.FIREPLACE;
		}

		public override string OnExamine(GameObject interactable)
		{
			if(!GameRegistry.INSTANCE.GetGameState("fire_out"))
				return "interactable.fireplace.examine.fire.txt";
			ScriptUIController.DisplayQuickEvent(QE_1);
			ScriptUIController.SetDisplayTextColor(Color.green);
			return "interactable.fireplace.examine.nofire.txt";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if(itemID == Items.WATER_BUCKET)
			{
				GameRegistry.INSTANCE.SetGameState("fire_out", true);
				GameObject.Destroy(spriteFire);
				Player.THE_PLAYER.RemoveInventoryItem(Items.WATER_BUCKET);
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.fireplace.usebucket.txt";
			}
			else if (itemID == Items.KEY_BOOK_1 || itemID == Items.KEY_BOOK_3 || itemID == Items.KEY_BOOK_4)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.fireplace.usebook.txt";
			}
			return base.OnUse(interactable, itemID);
		}
	}

	public class InteractableFireplaceTools : InteractableGeneric
	{
		private Sprite noPliers = null;

		public override void OnLevelInit(GameObject interactable)
		{
			if (noPliers == null)
				noPliers = Resources.Load<Sprite>("Texture/Interactable/fireplace_tools_np");

			if (GameRegistry.INSTANCE.GetGameState("found_pliers"))
				interactable.GetComponentInChildren<SpriteRenderer>().sprite = noPliers;
			
		}

		public override string GetInteractableName()
		{
			return "interactable.fireplacetools.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.FIREPLACE_TOOLS;
		}

		public override string OnExamine(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("found_pliers"))
				return "interactable.fireplacetools.nopliers.examine.txt";

			GameRegistry.INSTANCE.SetGameState("found_pliers", true);
			Player.THE_PLAYER.AddInventoryItem(Items.RUSTY_PLIERS, true);
			interactable.GetComponentInChildren<SpriteRenderer>().sprite = noPliers;
			ScriptUIController.SetDisplayTextColor(Color.green);
			return "interactable.fireplacetools.haspliers.examine.txt"; 
		}
	}

	public class InteractableGrandfatherClock : InteractableGeneric
	{
		private Sprite QE_1 = null;

		public override void OnLevelInit(GameObject interactable)
		{
			if (!GameRegistry.INSTANCE.GetGameState("lib_complete"))
			{
				interactable.GetComponentInChildren<Animator>().SetBool("broken", false);
				AudioManager.INSTANCE.PlayAudio( AudioClips.CLOCK_TICK, interactable, true);
			}

			if (QE_1 == null)
				QE_1 = Resources.Load<Sprite>("Texture/QuickEvent/qe_04_0");
		}

		public override string GetInteractableName()
		{
			return "interactable.grandfatherclock.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.GRANDFATHER_CLOCK;
		}

		public override string OnExamine(GameObject interactable)
		{
			if(GameRegistry.INSTANCE.GetGameState("lib_complete"))
			{
				ScriptUIController.DisplayQuickEvent(QE_1);
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.grandfatherclock.broken.examine.txt";
			}
			return "interactable.grandfatherclock.working.examine.txt";
		}
	}

	public class InteractableOfficeDrawers : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.officedrawers.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.OFFICE_DRAWERS;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.officedrawers.examine.txt";
		}
	}

	public class InteractableHiddenRoom : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.hiddenroom.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.HIDDEN_ROOM;
		}

		public override string OnExamine(GameObject interactable)
		{
			GameRegistry.INSTANCE.LoadLevel(Levels.LIBRARY_OFFICE, new Vector3(0f, -10.45f, -2f));
			return "";
		}


	}

	public class InteractableKitCellDoor : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.kitcelldoor.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.KIT_CELL_DOOR;
		}

		public override string OnExamine(GameObject interactable)
		{
			GameRegistry.INSTANCE.LoadLevel(Levels.CELLAR, new Vector3(-17f, -10.45f, -2f));
			return "";
		}
	}

	public class InteractableKitLibDoor : InteractableGeneric
	{
		private GameObject libKitDoorOpen = null;

		public override void OnLevelInit(GameObject interactable)
		{
			libKitDoorOpen = GameObject.Find("sprite_kit_lib_door");

			if (!GameRegistry.INSTANCE.GetGameState("kit_complete"))
				libKitDoorOpen.SetActive(false);
				
		}

		public override string GetInteractableName()
		{
			return "interactable.kitlibdoor.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.KIT_LIB_DOOR;
		}

		public override string OnExamine(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("kit_complete")) {
				AudioManager.INSTANCE.PlayAudio(AudioClips.WOODEN_DOOR_OPEN);
				GameRegistry.INSTANCE.LoadLevel(Levels.LIBRARY_RIGHT, new Vector3(17f, -10.45f, -2f));
				return "";
			}
			AudioManager.INSTANCE.PlayAudio(AudioClips.LOCKED);
			return "interactable.kitlibdoor.examine.txt";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if(itemID == Items.LIBRARY_KEY)
			{
				AudioManager.INSTANCE.PlayAudio(AudioClips.DOOR_UNLOCK);
				GameRegistry.INSTANCE.SetGameState("kit_complete", true);
				libKitDoorOpen.SetActive(true);
				Player.THE_PLAYER.RemoveInventoryItem(Items.LIBRARY_KEY);
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.kitlibdoor.usekey.txt";
			}

			return base.OnUse(interactable, itemID);
		}
	}

	public class InteractableCrate : InteractableGeneric
	{
		private readonly Vector3 moveDistance = new Vector3(5f, 0, 0);

		public override void OnLevelInit(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("crate_moved"))
			{
				GameObject.Destroy(interactable.GetComponent<Animator>());
				interactable.transform.position -= moveDistance;
			}
		}

		public override string GetInteractableName()
		{
			return "interactable.crate.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.CRATE;
		}

		public override string OnExamine(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("crate_moved"))
				return "interactable.crate.examine.post.txt";
			else if (GameRegistry.INSTANCE.GetGameState("checked_wheel"))
			{
				AudioManager.INSTANCE.PlayAudio(AudioClips.BOX_PUSH);
				GameRegistry.INSTANCE.SetGameState("crate_moved", true);
				interactable.GetComponent<Animator>().SetTrigger("Push");
				//interactable.transform.position -= moveDistance;
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.crate.examine.push.txt";
			}
			else
				return "interactable.crate.examine.txt";

		}
	}

	public class InteractableCheeseWheel : InteractableGeneric
	{
		private GameObject player;
		private readonly Vector3 crateTop = new Vector3(0, 5f, 0);

		public override void OnLevelInit(GameObject interactable)
		{
			player = GameObject.FindGameObjectWithTag("Player");
			if (GameRegistry.INSTANCE.GetGameState("took_wedge"))
				GameObject.Destroy(interactable);
		}

		public override string GetInteractableName()
		{
			return "interactable.cheese.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.CHEESE_WHEEL;
		}

		public override string OnExamine(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("crate_moved"))
			{
				player.transform.position += crateTop;
				ScriptPlayer.RESIZE = false;
				GameObject.Destroy(interactable);
				ScriptUIController.SetDisplayTextColor(Color.green);
				GameRegistry.INSTANCE.SetGameState("took_wedge", true);
				Player.THE_PLAYER.AddInventoryItem(Items.CHEESE);
				return "interactable.cheese.examine.postcrate.txt";
			}
			else if (!GameRegistry.INSTANCE.GetGameState("checked_wheel"))
				GameRegistry.INSTANCE.SetGameState("checked_wheel", true);
			return "interactable.cheese.examine.precrate.txt";
		}
	}

	public class InteractableCat : InteractableGeneric
	{
		private Sprite noKey = null;
		private SpriteRenderer catRenderer = null;
		private Sprite QE_1 = null;
		private Sprite QE_2 = null;

		public override void OnLevelInit(GameObject interactable)
		{
			if (noKey == null) 
				noKey = Resources.Load<Sprite>("Texture/Interactable/cat_nokey");

			if (QE_1 == null)
				QE_1 = Resources.Load<Sprite>("Texture/QuickEvent/qe_02_0");

			if (QE_2 == null)
				QE_2 = Resources.Load<Sprite>("Texture/QuickEvent/qe_02_1");

			catRenderer = interactable.GetComponentInChildren<SpriteRenderer>();

			if(GameRegistry.INSTANCE.GetGameState("took_key"))
				catRenderer.sprite = noKey;
		}

		public override string GetInteractableName()
		{
			return "interactable.cat.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.CAT;
		}

		public override string OnExamine(GameObject interactable)
		{
			return GameRegistry.INSTANCE.GetGameState("took_key") ? "interactable.cat.examine.postkey.txt" : "interactable.cat.examine.txt";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if (itemID == Items.DEAD_MOUSE)
			{
				Player.THE_PLAYER.RemoveInventoryItem(Items.DEAD_MOUSE);
				Player.THE_PLAYER.AddInventoryItem(Items.LIBRARY_KEY, true);
				GameRegistry.INSTANCE.SetGameState("took_key", true);
				catRenderer.sprite = noKey;
				ScriptUIController.DisplayQuickEvent(QE_1, QE_2);
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.cat.usemouse.txt";
			}

			return base.OnUse(interactable, itemID);
		}
	}

	public class InteractableMouseTrap : InteractableGeneric
	{
		public override void OnLevelInit(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("found_mousetrap"))
				GameObject.Destroy(interactable);
		}

		public override string GetInteractableName()
		{
			return "interactable.mousetrap.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.MOUSETRAP;
		}

		public override string OnExamine(GameObject interactable)
		{
			ScriptUIController.SetDisplayTextColor(Color.green);
			GameRegistry.INSTANCE.SetGameState("found_mousetrap", true);
			Player.THE_PLAYER.AddInventoryItem(Items.MOUSE_TRAP, true);
			GameObject.Destroy(interactable);
			return "interactable.mousetrap.examine.txt";
		}
	}

	public class InteractableRiggedMouseTrap : InteractableGeneric
	{
		//FIXME Not sure if this works
		private Sprite mouseless = null; 
		private Sprite trap = null;
		private SpriteRenderer trapRenderer = null;

		public override void OnLevelInit(GameObject interactable)
		{
			if(mouseless == null || trap == null)
			{
				mouseless = Resources.Load<Sprite>("Texture/Interactable/cheesetrap");
				trap = Resources.Load<Sprite>("Texture/Interactable/mousetrap");
			}

			trapRenderer = interactable.GetComponentInChildren<SpriteRenderer>();

			if (!GameRegistry.INSTANCE.GetGameState("mousetrap_set"))
			{
				trapRenderer.sprite = mouseless;
			}
			else if (GameRegistry.INSTANCE.GetGameState("took_mouse"))
				trapRenderer.sprite = trap;
			else
				AudioManager.INSTANCE.PlayAudio(AudioClips.MOUSETRAP_SNAP);
		}

		public override string GetInteractableName()
		{
			if (GameRegistry.INSTANCE.GetGameState("took_mouse"))
				return "interactable.mousetrap.name";
			return "interactable.cheesetrap.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.MOUSETRAP_RIGGED;
		}

		public override string OnExamine(GameObject interactable)
		{
			if (mouseless == trapRenderer.sprite)
				return "interactable.cheesetrap.examine.wait.txt";

			if (GameRegistry.INSTANCE.GetGameState("took_mouse"))
				return "interactable.cheesetrap.examine.post.txt";

			Player.THE_PLAYER.AddInventoryItem(Items.DEAD_MOUSE, true);
			GameRegistry.INSTANCE.SetGameState("took_mouse", true);
			trapRenderer.sprite = trap;
			ScriptUIController.SetDisplayTextColor(Color.green);
			return "interactable.mouse.examine.txt";

		}
	}

	public class InteractableMouseHole : InteractableGeneric
	{

		public override string GetInteractableName()
		{
			return "interactable.mousehole.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.MOUSE_HOLE;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.mousehole.examine.txt";
		}

		public override string OnUse(GameObject interactable, int itemID)
		{
			if (itemID == Items.RIGGED_MOUSE_TRAP)
			{
				ScriptUIController.SetDisplayTextColor(Color.green);
				Player.THE_PLAYER.RemoveInventoryItem(Items.RIGGED_MOUSE_TRAP);
				GameRegistry.INSTANCE.SetGameState("mousetrap_set", true);
				return "item.cheesetrap.place.txt";
			}
			else if (itemID == Items.MOUSE_TRAP)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.mousehole.usemousetrap.txt";
			}
			else if (itemID == Items.CHEESE)
			{
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.mousehole.usecheese.txt";
			}
			else
				return base.OnUse(interactable, itemID);

		}
	}

	public class InteractableShelfCheese : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.shelfcheese.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.SHELF_CHEESE;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.shelfcheese.examine.txt";
		}
	}

	public class InteractableShelf : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.shelf.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.SHELF;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.shelf.examine.txt";
		}
	}

	public class InteractableBarredCell : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.barredcell.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.BARRED_CELL;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.barredcell.examine.txt";
		}
	}

	public class InteractableCoatRack : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.coatrack.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.COAT_RACK;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.coatrack.examine.txt";
		}
	}

	public class InteractableCupboard : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.cupboard.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.CUPBOARD;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.cupboard.examine.txt";
		}
	}

	public class InteractableBucket : InteractableGeneric
	{
		public override void OnLevelInit(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("took_bucket"))
				GameObject.Destroy(interactable);

		}

		public override string GetInteractableName()
		{
			return "interactable.bucket.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.WATER_BUCKET;
		}

		public override string OnExamine(GameObject interactable)
		{
			GameRegistry.INSTANCE.SetGameState("took_bucket", true);
			Player.THE_PLAYER.AddInventoryItem(Items.WATER_BUCKET, true);
			GameObject.Destroy(interactable);
			ScriptUIController.SetDisplayTextColor(Color.green);
			return "interactable.bucket.examine.txt";
		}
	}

	public class InteractablePentagram : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.pentagram.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.PENTAGRAM;
		}

		public override string OnExamine(GameObject interactable)
		{
			return "interactable.pentagram.examine.txt";
		}
	}

	public class InteractablePuddle : InteractableGeneric
	{
		public override string GetInteractableName()
		{
			return "interactable.puddle.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.PUDDLE;
		}

		public override string OnExamine(GameObject obj)
		{
			return "interactable.puddle.examine.txt";
		}
	}

	public class InteractableGate : InteractableGeneric
	{
		private Sprite gateOpen = null;
		private SpriteRenderer gateRenderer = null;

		public override void OnLevelInit(GameObject interactable)
		{
			gateRenderer = interactable.GetComponentInChildren<SpriteRenderer>();

			if(gateOpen == null)
				gateOpen = Resources.Load<Sprite>("Texture/Interactable/gateopen");

			if (GameRegistry.INSTANCE.GetGameState("courtyard_complete"))
				gateRenderer.sprite = gateOpen;

		}

		public override string GetInteractableName()
		{
			return "interactable.gate.name";
		}

		public override int GetInteractableID()
		{
			return Interactables.GATE;
		}

		public override string OnExamine(GameObject interactable)
		{
			if (GameRegistry.INSTANCE.GetGameState("courtyard_complete"))
			{
				SceneManager.LoadScene(3);
				return "";
			}
			GameRegistry.INSTANCE.LoadLevel(Levels.LOCK, new Vector3(0f, 0f, -2f));
			return "interactable.gate.examine.txt";
		}
	}

	public class InteractableDialOne : InteractableGeneric
	{
		private Sprite[] runes = new Sprite[] { null, null, null, null, null };
		public int current = 0;

		public override void OnLevelInit(GameObject interactable)
		{
			for (int i = 0; i < 5; i++)
			{
				if (runes[i] == null)
					runes[i] = Resources.Load<Sprite>(string.Format("Texture/Interactable/Runes/rune_{0}", i));
			}

			current = 0;
		}

		public override int GetInteractableID()
		{
			return Interactables.DIAL_1;
		}

		public override string OnExamine(GameObject obj)
		{
			if (current >= 4)
				current = -1;
			AudioManager.INSTANCE.PlayAudio(AudioClips.LOCKTICK);
			obj.GetComponentInChildren<SpriteRenderer>().sprite = runes[++current];

			return "";
		}

	}

	public class InteractableDialTwo : InteractableGeneric
	{
		private Sprite[] runes = new Sprite[] { null, null, null, null, null };
		public int current = 0;

		public override void OnLevelInit(GameObject interactable)
		{
			for (int i = 0; i < 5; i++)
			{
				if (runes[i] == null)
					runes[i] = Resources.Load<Sprite>(string.Format("Texture/Interactable/Runes/rune_{0}", i));
			}

			current = 0;
		}

		public override int GetInteractableID()
		{
			return Interactables.DIAL_2;
		}

		public override string OnExamine(GameObject obj)
		{
			if (current >= 4)
				current = -1;
			AudioManager.INSTANCE.PlayAudio(AudioClips.LOCKTICK);
			obj.GetComponentInChildren<SpriteRenderer>().sprite = runes[++current];

			return "";
		}
	}

	public class InteractableDialThree : InteractableGeneric
	{
		private Sprite[] runes = new Sprite[] { null, null, null, null, null };
		public int current = 0;

		public override void OnLevelInit(GameObject interactable)
		{
			for (int i = 0; i < 5; i++)
			{
				if (runes[i] == null)
					runes[i] = Resources.Load<Sprite>(string.Format("Texture/Interactable/Runes/rune_{0}", i));
			}

			current = 0;
		}
		public override int GetInteractableID()
		{
			return Interactables.DIAL_3;
		}

		public override string OnExamine(GameObject obj)
		{
			if (current >= 4)
				current = -1;
			AudioManager.INSTANCE.PlayAudio(AudioClips.LOCKTICK);
			obj.GetComponentInChildren<SpriteRenderer>().sprite = runes[++current];

			return "";
		}
	}

	public class InteractableUnlockButton : InteractableGeneric
	{
		InteractableDialOne dialOne = null;
		InteractableDialTwo dialTwo = null;
		InteractableDialThree dialThree = null;

		public override void OnLevelInit(GameObject interactable)
		{
			if (dialOne == null)
				dialOne = (InteractableDialOne) GameRegistry.INSTANCE.GetInteractable(Interactables.DIAL_1);
			if (dialTwo == null)
				dialTwo = (InteractableDialTwo)GameRegistry.INSTANCE.GetInteractable(Interactables.DIAL_2);
			if (dialThree == null)
				dialThree = (InteractableDialThree)GameRegistry.INSTANCE.GetInteractable(Interactables.DIAL_3);
		}

		public override int GetInteractableID()
		{
			return Interactables.UNLOCK_BUTTON;
		}

		public override string OnExamine(GameObject obj)
		{
			int D1 = dialOne.current;
			int D2 = dialTwo.current;
			int D3 = dialThree.current;

			if(D1 == 4 && D2 == 1 && D3 == 3)
			{
				GameRegistry.INSTANCE.SetGameState("courtyard_complete", true);
				GameRegistry.INSTANCE.LoadLevel(Levels.COURTYARD, new Vector3(0f, -7.25f, -2f));
				ScriptUIController.SetDisplayTextColor(Color.green);
				return "interactable.lockbtn.succeed.txt";
			}
			else
			{
				AudioManager.INSTANCE.PlayAudio(AudioClips.LOCKED);
				ScriptUIController.SetDisplayTextColor(Color.red);
				return "interactable.lockbtn.fail.txt";
			}
		}
	}

	public class InteractableChair : InteractableGeneric
	{
		public override int GetInteractableID()
		{
			return Interactables.CHAIR;
		}

		public override string GetInteractableName()
		{
			return "interactable.chair.name";
		}

		public override string OnExamine(GameObject obj)
		{
			return "interactable.chair.examine.txt";
		}
	}

	public class InteractableTable : InteractableGeneric
	{
		public override int GetInteractableID()
		{
			return Interactables.TABLE;
		}

		public override string GetInteractableName()
		{
			return "interactable.table.name";
		}

		public override string OnExamine(GameObject obj)
		{
			return "interactable.table.examine.txt";
		}
	}

	public class InteractableReturn : InteractableGeneric
	{
		public override int GetInteractableID()
		{
			return Interactables.RETURN_TXT;
		}

		public override string GetInteractableName()
		{
			return "interactable.return.name";
		}

		public override string OnExamine(GameObject obj)
		{
			return "interactable.return.examine.txt";
		}
	}
}

