﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using YMAudio;
using YMLib;
using YMLevel;
using YMInteractable;
using YMItem;

public class ScriptMenu : MonoBehaviour {

	void Awake()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		if (!YMLevels.initialized)
			YMLevels.Init();
		if (!YMInteractables.initialized)
			YMInteractables.Init();
		if (!YMItems.initialized)
			YMItems.Init();
	}

	// Use this for initialization
	void Start ()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.BG[1], true);
	}
		
	// Update is called once per frame
	void Update () {
	}

	public void Play()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		SceneManager.LoadScene(2);
	}
}
