﻿using UnityEngine;
using System.Collections;
using YMLevel;

public class ScriptInputController : MonoBehaviour {

	ScriptPlayer ScriptPlayerObj;

	// Use this for initialization
	void Start () {
		ScriptPlayerObj = GameObject.FindWithTag("Player").GetComponent<ScriptPlayer>();
	}
	
	// Update is called once per frame
	void Update () {

		/*
		 * General warning:
		 * I have no idea how the "UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()" function works.
		 * All I know is that it blocks movement if there is anything on the UI.
		 * If something input related breaks its probably because of that.
		 * Hey, don't blame me. If you want to figure this out, go do it yourself.
		 * 
		 * --A hard working code-monkey
		 */

		if (Input.GetKeyDown(KeyCode.Mouse0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && Input.touchCount == 0)
			OnTouch(Input.mousePosition);
		else {
			foreach (Touch touch in Input.touches)
			{
				if (touch.phase == TouchPhase.Began && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(touch.fingerId))
					OnTouch(touch.position);
			}
		}


	
	}

	private void OnTouch(Vector3 pos) {
		//Debug.Log("Processing touch at " + pos);
		Camera cam = Camera.main;
		RaycastHit2D hit = Physics2D.Raycast(new Vector2(cam.ScreenToWorldPoint(pos).x,cam.ScreenToWorldPoint(pos).y), Vector2.zero, 0f);
		if (hit)
		{
			if (hit.collider && hit.collider.gameObject.tag == "Level")
			{
				GameObject playerObj = GameObject.FindWithTag("Player");
				playerObj.GetComponent<ScriptPlayer>().MoveTo(hit.point, null);
			}
			else if (hit.collider && hit.collider.gameObject.tag == "Interactable")
			{
				//Debug.Log("Hit an interactable");
				GameObject interactable = hit.collider.gameObject;
				ScriptPlayerObj.MoveTo(interactable.transform.position, interactable);
			}
		}
	}
}
