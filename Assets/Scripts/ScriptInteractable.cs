﻿using UnityEngine;
using System.Collections;
using YMLevel;

public class ScriptInteractable : MonoBehaviour {

	public int interactableID;
	public IInteractable theInteractable;

	// Use this for initialization
	void Awake () {
		theInteractable = GameRegistry.INSTANCE.GetInteractable(interactableID);
		if (theInteractable == null)
			Debug.Log("[Error] Failed to find object for the id: " + interactableID);
		theInteractable.OnLevelInit(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		theInteractable.OnTick(gameObject);
	}

	public string OnInteractableUse(int itemID)
	{
		return theInteractable.OnUse(gameObject, itemID);
	}

	public string OnInteractableExamine()
	{
		return theInteractable.OnExamine(gameObject);
	}

	//public void OnInteractableInit()
	//{
		//if (theInteractable == null)
			//theInteractable = GameRegistry.INSTANCE.GetInteractable(interactableID);
		//theInteractable.OnLevelInit(gameObject);
	//}
}
