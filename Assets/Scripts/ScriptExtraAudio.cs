﻿using UnityEngine;
using System.Collections;
using YMAudio;
using YMUtils;
using YMLib;

public class ScriptExtraAudio : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void GuardCall()
	{
		AudioManager.INSTANCE.PlayAudio(AudioUtils.PlayRandom(AudioClips.GUARD));
	}

	void CloseDoor()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.WOODEN_DOOR_CLOSE);
	}
}
