﻿using UnityEngine;
using System.Collections;
using YMLevel;
using UnityEngine.UI;
using YMUtils;
using YMPlayer;
using System.Collections.Generic;
using YMLib;
using YMAudio;
using UnityEngine.SceneManagement;

public class ScriptUIController : MonoBehaviour {

	private static GameObject buttonLeft;
	private static GameObject buttonRight;
	private static GameObject textDisplay;
	private static GameObject uiInventory;
	private static GameObject uiQuickEvent;
	private static Animator darkout;
	private static GameObject uipause;
	public static bool QUICK_EVENT_ON = false;
	private static float quickEventTimer = 0f;
	private static GameObject textUIInv;
	private static Image quickEventImage;
	private static Sprite[] eventQueue;
	private static AudioClip[] soundQueue;
	private static int nextSprite = 0;
	public static int selectedItem;
	public static bool INVENTORY_OPEN;
	private static Color invTextColor;
	private static Color dispTextColor;
	private static bool invTextSetColor = false;
	private static bool dispTextSetColor = false;

	public static EnumInventoryFunction INV_FUNC_MODE = EnumInventoryFunction.NONE;

	private static GameObject[] itemDisplay;
	private static readonly int inventoryCurrentMax = 4;

	private static GameObject invFeedbackIcon;

	// Use this for initialization
	void Start () {
		darkout = GameObject.Find("darkoutPanel").GetComponent<Animator>();
		invFeedbackIcon = GameObject.Find("InvFeedback");
		buttonLeft = GameObject.Find("MoveLeftButton");
		buttonRight = GameObject.Find("MoveRightButton");
		textDisplay = GameObject.Find("DisplayText");
		uiInventory = GameObject.Find("UIInventory");
		textUIInv = GameObject.Find("TextUIInventory");
		uiQuickEvent = GameObject.Find("UIQuickEvent");
		uipause = GameObject.Find("UIPause");
		quickEventImage = GameObject.Find("QuickEventImage").GetComponent<Image>();
		
		selectedItem = -1;

		LinkInventory();

		uiInventory.SetActive(false);
		uiQuickEvent.SetActive(false);
		uipause.SetActive(false);
	}

	private void LinkInventory()
	{
		itemDisplay = new GameObject[4];
		for (int i = 0; i < 4; i++)
		{
			itemDisplay[i] = GameObject.Find(string.Format("Item{0}", i));
		}
	}

	// Update is called once per frame
	void Update()
	{	
		UpdateUIElements();
	}

	private void UpdateUIElements()
	{

		if (INVENTORY_OPEN)
		{
			int[] invDis = Player.THE_PLAYER.GetInventory();
			for (int i = 0; i < inventoryCurrentMax; i++)
			{
				if (invDis[i] == -1)
					itemDisplay[i].SetActive(false);
				else {
					itemDisplay[i].SetActive(true);
					Button itemBtn = itemDisplay[i].GetComponent<Button>();
					itemBtn.image.sprite = GameRegistry.INSTANCE.GetItem(invDis[i]).GetItemSprite();
					if (INV_FUNC_MODE == EnumInventoryFunction.NONE)
						itemBtn.interactable = false;
					else
						itemBtn.interactable = true;
				}
			}
		}

		if (QUICK_EVENT_ON)
		{
			uiQuickEvent.SetActive(true);
			if (quickEventTimer <= 0f)
			{
				if(nextSprite == eventQueue.Length)
				{
					QUICK_EVENT_ON = false;
					uiQuickEvent.SetActive(false);
					soundQueue = null;
					eventQueue = null;
				}

				if (soundQueue != null)
					AudioManager.INSTANCE.PlayAudio(soundQueue[nextSprite]);

				if(eventQueue != null)
					quickEventImage.sprite = eventQueue[nextSprite++];

				quickEventTimer = 1.5f;

				if (eventQueue == null)
					quickEventTimer = 0;
			}
			else
				quickEventTimer -= Time.deltaTime;
		}

		GameObject level = GameObject.FindWithTag("Level");
		if (level == null)
			return;
		ScriptLevelInfo LevelInfo = level.GetComponent<ScriptLevelInfo>();
		if (LevelInfo == null)
			return;

		//for (int i = 0; i < 100; i++)
		//{

		//}

		buttonLeft.SetActive(LevelInfo.showLeftButton);
		buttonRight.SetActive(LevelInfo.showRightButton);
	}

	public void OnPause()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		uipause.SetActive(true);
	}

	public void OnResume()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		uipause.SetActive(false);
	}

	public void OnQuit()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		SceneManager.LoadScene(0);
	}

	public void OnRightLevelChange()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		ScriptLevelInfo LevelInfo = GameObject.FindWithTag("Level").GetComponent<ScriptLevelInfo>();
		ScriptPlayer.RESIZE = true;
		if (LevelInfo == null)
			return;

		GameRegistry.INSTANCE.LoadLevel(LevelInfo.rightLocID, LevelInfo.GetNextStartPos());

	}

	public void OnLeftLevelChange()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		ScriptLevelInfo LevelInfo = GameObject.FindWithTag("Level").GetComponent<ScriptLevelInfo>();
		ScriptPlayer.RESIZE = true;
		if (LevelInfo == null)
			return;

		GameRegistry.INSTANCE.LoadLevel(LevelInfo.leftLocID, LevelInfo.GetNextStartPos());
	}

	public static void SetDisplayTextColor(Color color)
	{
		//HACK Don't blame me, you'll be doing the same thing too if you want to save time
		if (color == Color.red)
		{
			AudioManager.INSTANCE.PlayAudio(AudioClips.UI_FAIL);
		}
		else if (color == Color.green)
		{
			AudioManager.INSTANCE.PlayAudio(AudioClips.UI_PICKUP);
		}

		dispTextColor = color;
		dispTextSetColor = true;
	}

	public static void UpdateDisplayText(string format, params string[] unlocalized)
	{
		string localized = LanguageMapping.INSTANCE.TranslateFormatted(format, unlocalized);
		if (textDisplay == null)
			return;
		Text txtObj = textDisplay.GetComponent<Text>();
		txtObj.text = localized;
		if (dispTextSetColor)
		{
			txtObj.color = dispTextColor;
			dispTextSetColor = false;
		}
		else
		{
			txtObj.color = Color.white;
		}
	}

	public static void UpdateDisplayText(string unlocalized)
	{
		string localized = LanguageMapping.INSTANCE.Translate(unlocalized);
		if (textDisplay == null)
			return;
		Text txtObj = textDisplay.GetComponent<Text>();
		txtObj.text = localized;
		if(dispTextSetColor)
		{
			txtObj.color = dispTextColor;
			dispTextSetColor = false;
		}
		else
		{
			txtObj.color = Color.white;
		}
	}



	public static void SetInventoryTextColor(Color color)
	{   
		//HACK Don't blame me, you'll be doing the same thing too if you want to save time
		if (color == Color.red)
		{
			AudioManager.INSTANCE.PlayAudio(AudioClips.UI_FAIL);
		}
		else if (color == Color.green)
		{
			AudioManager.INSTANCE.PlayAudio(AudioClips.UI_SUCCEED);
		}

		invTextColor = color;
		invTextSetColor = true;
	}

	public static void UpdateInventoryText(string format, params string[] unlocalized)
	{
		string localized = LanguageMapping.INSTANCE.TranslateFormatted(format, unlocalized);
		if (textUIInv== null)
			return;
		Text txtObj = textUIInv.GetComponent<Text>();
		txtObj.text = localized;
		if (invTextSetColor)
		{
			txtObj.color = invTextColor;
			invTextSetColor = false;
		}
		else
		{
			txtObj.color = Color.black;
		}
	}

	public static void UpdateInventoryText(string unlocalized)
	{
		string localized = LanguageMapping.INSTANCE.Translate(unlocalized);
		if (textUIInv == null)
			return;
		Text txtObj = textUIInv.GetComponent<Text>();
		txtObj.text = localized;
		if (invTextSetColor)
		{
			txtObj.color = invTextColor;
			invTextSetColor = false;
		}
		else
		{
			txtObj.color = Color.black;
		}
	}

	public void OnInventoryOpen()
	{
		selectedItem = -1;
		INV_FUNC_MODE = EnumInventoryFunction.NONE;
		SetShowInventoryUI(true);
	}

	public void OnInventoryClose()
	{
		if (INV_FUNC_MODE == EnumInventoryFunction.COMBINE)
			selectedItem = -1;
		SetShowInventoryUI(false);
	}

	public static void SetShowInventoryUI(bool show)
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		UpdateInventoryText("gui.inventory.displaytextdefault.txt");
		uiInventory.SetActive(show);
		INVENTORY_OPEN = show;
	}

	public void OnInventorySelectItem(int inventoryID)
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		int itemID = Player.THE_PLAYER.GetInventory()[inventoryID];
		switch (INV_FUNC_MODE)
		{
//            case EnumInventoryFunction.NONE:
//                UpdateInventoryText(GameRegistry.INSTANCE.GetItem(itemID).GetItemName());
//                break;
			case EnumInventoryFunction.EXAMINE:
				UpdateInventoryText(GameRegistry.INSTANCE.GetItem(itemID).OnExamine());
				INV_FUNC_MODE = EnumInventoryFunction.NONE;
				break;
			case EnumInventoryFunction.USE:
				UpdateDisplayText("format.use.txt", GameRegistry.INSTANCE.GetItem(itemID).GetItemName(), "misc.none.txt");
				selectedItem = itemID;
				OnInventoryClose();
				break;
			case EnumInventoryFunction.COMBINE:
				if (selectedItem == -1)
				{
					selectedItem = itemID;
					UpdateInventoryText("format.combine.txt", GameRegistry.INSTANCE.GetItem(itemID).GetItemName());
				}
				else
				{
					UpdateInventoryText(GameRegistry.INSTANCE.GetItem(itemID).OnCombine(selectedItem));
					selectedItem = -1;
					INV_FUNC_MODE = EnumInventoryFunction.NONE;
				}
				break;
		}
	}

	public void OnInventoryExamine()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		if (Player.THE_PLAYER.GetInventory()[0] != -1)
		{
			INV_FUNC_MODE = EnumInventoryFunction.EXAMINE;
			UpdateInventoryText("gui.inventory.examine.description.txt");
		}
		else
		{
			ScriptUIController.SetInventoryTextColor(Color.red);
			UpdateInventoryText("gui.inventory.examine.needmore.txt");
		}
	}

	public void OnInventoryCombine()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		if (Player.THE_PLAYER.GetInventory()[1] != -1)
		{
			INV_FUNC_MODE = EnumInventoryFunction.COMBINE;
			UpdateInventoryText("gui.inventory.combine.description.txt");
		}
		else
		{
			ScriptUIController.SetInventoryTextColor(Color.red);
			UpdateInventoryText("gui.inventory.combine.needmore.txt");
		}
	}

	public void OnInventoryUse()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		if (Player.THE_PLAYER.GetInventory()[0] != -1)
		{
			INV_FUNC_MODE = EnumInventoryFunction.USE;
			UpdateInventoryText("gui.inventory.use.description.txt");
		}
		else
		{
			ScriptUIController.SetInventoryTextColor(Color.red);
			UpdateInventoryText("gui.inventory.use.needmore.txt");
		}
	}

	public static void DisplayQuickEvent(params Sprite[] scenes)
	{
		eventQueue = scenes;
		nextSprite = 0;
		
		QUICK_EVENT_ON = true;
	}

	public static void SetAudioOnEventChange(params AudioClip[] clips)
	{
		soundQueue = clips;
	}

	public static void ShowInventoryPickupFeedback(int itemID)
	{
		Image theImage = invFeedbackIcon.GetComponent<Image>();
		theImage.sprite = GameRegistry.INSTANCE.GetItem(itemID).GetItemSprite();
		Animator theAnimator = invFeedbackIcon.GetComponent<Animator>();
		theAnimator.SetTrigger("StartFeedback");
	}

	public static void AddDarkoutAnimation()
	{
		if (darkout == null)
			darkout = GameObject.Find("darkoutPanel").GetComponent<Animator>();
		darkout.SetTrigger("Blackout");
	}

}
