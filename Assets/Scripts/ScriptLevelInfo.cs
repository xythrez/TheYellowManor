﻿using UnityEngine;

public class ScriptLevelInfo : MonoBehaviour {

	public int levelID = 0;
	public bool showLeftButton = false;
	public bool showRightButton = false;
	public int leftLocID = 0;
	public int rightLocID = 0;
	public float playerX = 0;
	public float playerY = 0;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public Vector2 GetNextStartPos()
	{
		return new Vector3(playerX, playerY, -2f);
	}
}
