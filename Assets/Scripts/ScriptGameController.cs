﻿using UnityEngine;
using YMLevel;
using YMInteractable;
using YMItem;
using YMAudio;
using YMLib;
using YMPlayer;

public class ScriptGameController : MonoBehaviour
{
	private AudioSource bgm = null;
	private int oldlvl = 0;

	void Awake()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		if (!YMLevels.initialized)
			YMLevels.Init();
		if(!YMInteractables.initialized)
			YMInteractables.Init();
		if (!YMItems.initialized)
			YMItems.Init();
	}

	// Use this for initialization
	void Start () {
		GameRegistry.INSTANCE.ClearGameState();
		Player.InitNewPlayer();
		//Non-camera
		bgm = AudioManager.INSTANCE.PlayAudio(AudioClips.BG[0], true);
		GameRegistry.INSTANCE.LoadLevel(Levels.CELLAR, new Vector3(16f, -10.45f, -2f));

	}

	void Update()
	{
		GameObject level = GameObject.FindGameObjectWithTag("Level");

		if (level == null)
			return;

		ScriptLevelInfo levelinfo = level.GetComponent<ScriptLevelInfo>();

		if (levelinfo == null)
			return;

		int levelid = levelinfo.levelID;

		if(levelid < 3 && oldlvl >= 3)
		{
			Destroy(bgm.gameObject);
			bgm = AudioManager.INSTANCE.PlayAudio(AudioClips.BG[0], true);
		}
		else if(levelid >= 3 && oldlvl < 3)
		{
			Destroy(bgm.gameObject);
			bgm = AudioManager.INSTANCE.PlayAudio(AudioClips.BG[1], true);
		}

		oldlvl = levelid;
	}

	
	


}
