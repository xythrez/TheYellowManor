﻿using System.Collections;
using UnityEngine;
using YMLevel;

public class InteractableControllerCheeseTrap : MonoBehaviour {

	private GameObject cheeseTrap;

	// Use this for initialization
	void Start () {
		cheeseTrap = GameObject.Find("Cheesetrap");
		cheeseTrap.SetActive(false);
	
	}
	
	// Update is called once per frame
	void Update () {

		if (GameRegistry.INSTANCE.GetGameState("mousetrap_set"))
		{
			cheeseTrap.SetActive(true);
			Destroy(gameObject);
		}
	
	}
}
