﻿using UnityEngine;
using System.Collections;
using YMLevel;

public class InteractableControllerGuard : MonoBehaviour {

	private GameObject guard;

	// Use this for initialization
	void Start () {
		guard = GameObject.Find("Guard");
		guard.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (GameRegistry.INSTANCE.GetGameState("cellar_complete"))
		{
			guard.SetActive(true);
			Destroy(gameObject);
		}
	}
}
