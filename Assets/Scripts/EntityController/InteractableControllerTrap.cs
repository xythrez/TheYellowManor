﻿using UnityEngine;
using System.Collections;
using YMLevel;

public class InteractableControllerTrap : MonoBehaviour {

	private GameObject trap;

	// Use this for initialization
	void Start () {
		trap = GameObject.Find("RopeTrap");
	}
	
	// Update is called once per frame
	void Update () {
		if(GameRegistry.INSTANCE.GetGameState("cellar_complete"))
		{
			Destroy(trap);
			Destroy(gameObject);
		}
		else if (GameRegistry.INSTANCE.GetGameState("trap_set"))
		{
			trap.SetActive(true);
			Destroy(gameObject);
		}
		else
			trap.SetActive(false);
	}
}
