﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using YMAudio;
using YMLib;

public class ScriptEnding : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}
		
	// Update is called once per frame
	void Update () {
	}

	public void Next()
	{
		AudioManager.INSTANCE.PlayAudio(AudioClips.UI_BUTTON);
		SceneManager.LoadScene(0);
	}
}
